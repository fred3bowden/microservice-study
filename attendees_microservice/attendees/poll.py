import json
import requests

from .models import ConferenceVO


def get_conferences():
    url = "http://monolith:8000/api/conferences/"  # url to conference data in monolith
    response = requests.get(url)  # get data
    content = json.loads(
        response.content
    )  # take the content json string and turn into a dict

    for conference in content[
        "conferences"
    ]:  # for each conference object in list of conferences
        ConferenceVO.objects.update_or_create(
            import_href=conference["href"],  # outside of defaults is creation
            defaults={
                "name": conference["name"]
            },  # updating from the conference instance in monolith
        )
